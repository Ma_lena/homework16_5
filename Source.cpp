#include <iostream>
#include <Windows.h>
#include <ctime>
#include <string>

using namespace std;

int main()
{
	SYSTEMTIME st;
	GetSystemTime(&st);
	//std::cout <<st.wDay;

	const int N = 4;
	int array[N][N];
	int A;
	A = st.wDay % N;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;

			cout << array[i][j] << "\t";
		}
		cout << endl;
	}
	int sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += array[A][i];
	}
	cout << sum << endl;
}
